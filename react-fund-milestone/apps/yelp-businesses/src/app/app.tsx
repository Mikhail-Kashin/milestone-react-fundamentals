import { Components } from '@react-fund-milestone/components';

export function App() {
  return (
    <div>
      <Components />
    </div>
  );
}

export default App;
