import { render, screen } from '@testing-library/react';
import { mockShappedBusinessObject } from '@react-fund-milestone/test-data';

import BusinessCard from './business-card';

const mockHandleFavorite = jest.fn();

describe('BusinessDetail', () => {
  it('should render a card with name, image, number, rating and status if closed or not', () => {
    const { baseElement } = render(
      <BusinessCard
        business={mockShappedBusinessObject}
        handleFavorite={mockHandleFavorite}
      />
    );
    expect(baseElement).toBeTruthy();

    expect(screen.getByText(/Mock st/i)).toBeTruthy();
    expect(screen.getByText(/mockname/i)).toBeTruthy();
    expect(screen.getByText(/mocknumber/i)).toBeTruthy();
    expect(screen.getByText(/999/i)).toBeTruthy();
  });
});
