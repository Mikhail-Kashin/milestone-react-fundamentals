import FavoriteButton from '../favorite-button/favorite-button';

import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Grid,
  IconButton,
  Stack,
} from '@mui/material';

import StarsIcon from '@mui/icons-material/Stars';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';

import { shapedYelpBusinessObject } from '@react-fund-milestone/types';

export interface BusinessDetailProps {
  business: shapedYelpBusinessObject;
  handleFavorite: (value: shapedYelpBusinessObject) => void;
}

export function BusinessCard({
  business: { displayAddress, id, image, isClosed, name, phone, rating },
  business,
  handleFavorite,
}: BusinessDetailProps) {
  return (
    <Grid item justifyContent="center">
      <Card sx={{ width: 425, height: 470 }}>
        <CardMedia
          component="img"
          height="194"
          image={`${image}?w=248&fit=crop&auto=format`}
          alt={name}
        />
        <Stack direction="row" alignItems="center">
          <CardHeader title={name} titleTypographyProps={{ variant: 'h6' }} />
          <FavoriteButton handleFavorite={handleFavorite} business={business} />
        </Stack>
        <CardContent>
          <div>
            <p>
              Address: {displayAddress[0]} {displayAddress[1]}
            </p>
            <p>Phone: {phone}</p>
            <p>Store is Currently: {isClosed ? 'CLOSED' : 'Open'}</p>
            <Stack direction="row" alignItems="center" spacing={1}>
              <StarsIcon />
              <p> {rating}</p>
            </Stack>
          </div>
        </CardContent>
      </Card>
    </Grid>
  );
}

export default BusinessCard;
