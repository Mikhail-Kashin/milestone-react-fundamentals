import BusinessesItems from './businesses-items/businesses-items';
import SearchInput from './search-input/search-input';

import {
  searchResult,
  shappedData,
  addRemoveFavorites,
} from '@react-fund-milestone/utilities';
import { shapedYelpBusinessObject } from '@react-fund-milestone/types';
import { useState, useEffect } from 'react';

export function Components() {
  const [currentBusinesses, setCurrentBusinesses] =
    useState<shapedYelpBusinessObject[]>(shappedData);
  const [currentSearch, setCurrentSearch] = useState<string>('');
  const [currentFavorites, setCurrentFavorites] = useState<
    shapedYelpBusinessObject[]
  >([]);

  const handleCurrentSearch = (search: string) => {
    setCurrentSearch(search);
  };

  const handleFavorite = (newFavorite: shapedYelpBusinessObject) => {
    setCurrentFavorites(addRemoveFavorites(currentFavorites, newFavorite));
  };

  useEffect(() => {
    const handleCurrentDisplayedBusinesses = () => {
      const businesses: shapedYelpBusinessObject[] =
        searchResult(currentSearch);
      return businesses.length > 0
        ? setCurrentBusinesses(businesses)
        : setCurrentBusinesses(shappedData);
    };
    handleCurrentDisplayedBusinesses();
  }, [currentSearch]);

  return (
    <div>
      <SearchInput
        currentSearch={currentSearch}
        handleSearch={handleCurrentSearch}
      />
      <BusinessesItems
        businesses={currentBusinesses}
        handleFavorite={handleFavorite}
      />
    </div>
  );
}

export default Components;
