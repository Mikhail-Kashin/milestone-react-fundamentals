import { render, screen } from '@testing-library/react';

import Components from './components';

describe('Components', () => {
  it('should render search input, and businesses as well as favorite button', () => {
    const { baseElement } = render(<Components />);
    const favoriteButtons = screen.getAllByRole('button');
    const searchInput = screen.getByRole('searchbox');

    expect(baseElement).toBeTruthy();
    expect(screen.getByText(/Maple Street/i)).toBeTruthy();
    expect(favoriteButtons).toBeTruthy();
    expect(searchInput).toBeTruthy();
  });
});
