import { render, screen } from '@testing-library/react';
import { mockShappedBusinessArray } from '@react-fund-milestone/test-data';

import BusinessesItems from './businesses-items';

const mockHandleFavorite = jest.fn();
describe('BusinessesList', () => {
  it('should render multiple businesses', () => {
    const { baseElement } = render(
      <BusinessesItems
        businesses={mockShappedBusinessArray}
        handleFavorite={mockHandleFavorite}
      />
    );
    expect(baseElement).toBeTruthy();
    expect(screen.getByText(/mockname/i)).toBeTruthy();
    expect(screen.getByText(/mocknumber/i)).toBeTruthy();
  });
});
