import BusinessCard from '../business-card/business-card';
import { shapedYelpBusinessObject } from '@react-fund-milestone/types';

import { Grid } from '@mui/material';
// import { shappedData } from '@react-fund-milestone/utilities';

interface BusinessItemsProps {
  businesses: shapedYelpBusinessObject[];
  handleFavorite: (value: shapedYelpBusinessObject) => void;
}

const BusinessesItems = ({
  businesses,
  handleFavorite,
}: BusinessItemsProps) => {
  const mappedBusinesses = businesses.map((business) => {
    return (
      <BusinessCard
        key={business.id}
        business={business}
        handleFavorite={handleFavorite}
      />
    );
  });

  return (
    <Grid container spacing={2}>
      {mappedBusinesses}
    </Grid>
  );
};

export default BusinessesItems;
