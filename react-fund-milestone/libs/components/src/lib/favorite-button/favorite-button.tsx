import { IconButton } from '@mui/material';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { shapedYelpBusinessObject } from '@react-fund-milestone/types';

interface FavoriteButtonProps {
  business: shapedYelpBusinessObject;
  handleFavorite: (value: shapedYelpBusinessObject) => void;
}

export function FavoriteButton({
  business,
  handleFavorite,
}: FavoriteButtonProps) {
  const favoriteStatus = business.isFavorite;
  return (
    <IconButton onClick={() => handleFavorite(business)}>
      {favoriteStatus ? <FavoriteIcon color="error" /> : <FavoriteBorderIcon />}
    </IconButton>
  );
}

export default FavoriteButton;
