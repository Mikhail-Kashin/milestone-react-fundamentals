import { render, screen } from '@testing-library/react';
import { mockShappedBusinessObject } from '@react-fund-milestone/test-data';

import FavoriteButton from './favorite-button';
import userEvent from '@testing-library/user-event';

const mockHandleFavorite = jest.fn();
describe('FavoriteButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <FavoriteButton
        business={mockShappedBusinessObject}
        handleFavorite={mockHandleFavorite}
      />
    );
    const favoriteButton = screen.getByRole('button');

    expect(baseElement).toBeTruthy();
    expect(favoriteButton).toBeTruthy();
  });

  it('should call mock handle when user clicks button', () => {
    const { baseElement } = render(
      <FavoriteButton
        business={mockShappedBusinessObject}
        handleFavorite={mockHandleFavorite}
      />
    );
    const favoriteButton = screen.getByRole('button');

    userEvent.click(favoriteButton);
    userEvent.click(favoriteButton);

    expect(baseElement).toBeTruthy();
    expect(mockHandleFavorite).toBeCalledTimes(2);
  });
});
