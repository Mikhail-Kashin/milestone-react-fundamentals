import { render, screen } from '@testing-library/react';

import SearchInput from './search-input';
import userEvent from '@testing-library/user-event';

describe('SearchInput', () => {
  const mockHandleSearch = jest.fn();
  it('should render successfully', () => {
    const { baseElement } = render(
      <SearchInput handleSearch={mockHandleSearch} currentSearch={''} />
    );
    expect(baseElement).toBeTruthy();
    const searchInput = screen.getByRole('searchbox');
    expect(searchInput).toBeTruthy();
  });

  it('should allow values to be in search box', () => {
    const { baseElement } = render(
      <SearchInput handleSearch={mockHandleSearch} currentSearch={'text'} />
    );
    expect(baseElement).toBeTruthy();
    const search = screen.getByDisplayValue('text');
  });

  it('should allow user to type a search', () => {
    const { baseElement } = render(
      <SearchInput handleSearch={mockHandleSearch} currentSearch={''} />
    );

    const searchInput = screen.getByRole('searchbox');

    userEvent.type(searchInput, 'words');

    expect(baseElement).toBeTruthy();
    expect(mockHandleSearch).toBeCalledTimes(5);
  });
});
