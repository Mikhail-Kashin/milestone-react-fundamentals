import { Box, TextField } from '@mui/material';

interface SearchInputProps {
  handleSearch: (value: string) => void;
  currentSearch: string;
}

export const SearchInput = ({
  handleSearch,
  currentSearch,
}: SearchInputProps) => {
  return (
    <Box>
      <TextField
        id="standard-search"
        label="Search field"
        type="search"
        variant="outlined"
        fullWidth={true}
        margin="normal"
        onChange={(e) => handleSearch(e.target.value)}
        value={currentSearch}
      />
    </Box>
  );
};

export default SearchInput;
