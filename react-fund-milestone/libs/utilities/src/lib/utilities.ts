import { jacksonvilleBuisnessesRawData } from '@react-fund-milestone/mock-data';

import {
  yelpBusinessObject,
  shapedYelpBusinessObject,
} from '@react-fund-milestone/types';

export const jacksonvilleBuisnessesArray =
  jacksonvilleBuisnessesRawData.businesses;

export const jacksonvilleBuisnessesArrayWithIsFavorite =
  jacksonvilleBuisnessesArray.map((business) => {
    return { ...business, isFavorite: false };
  });

export const shapeYelpArray = (
  yelpArray: yelpBusinessObject[]
): shapedYelpBusinessObject[] => {
  const shapped = yelpArray.map(
    ({
      location: { display_address: displayAddress },
      id,
      image_url: image,
      is_closed: isClosed,
      name,
      phone,
      rating,
      isFavorite,
    }) => {
      return {
        displayAddress,
        id,
        image,
        isClosed,
        name,
        phone,
        rating,
        isFavorite,
      };
    }
  );
  return shapped;
};

export const shappedData = shapeYelpArray(
  jacksonvilleBuisnessesArrayWithIsFavorite
);

export const searchResult = (keyWord: string) => {
  return shappedData.filter((business) => {
    const lowerKeyWord = keyWord.toLowerCase();
    const lowerName = business.name.toLowerCase();
    return lowerName.includes(lowerKeyWord);
  });
};

const removeFavorite = (
  businesses: shapedYelpBusinessObject[],
  filter: shapedYelpBusinessObject
) => {
  return businesses.filter((business) => {
    return business !== filter;
  });
};

export const addRemoveFavorites = (
  favoriteBusinesses: shapedYelpBusinessObject[],
  newFavorite: shapedYelpBusinessObject
) => {
  if (favoriteBusinesses.includes(newFavorite)) {
    newFavorite.isFavorite = false;
    return removeFavorite(favoriteBusinesses, newFavorite);
  }
  newFavorite.isFavorite = true;
  return [...favoriteBusinesses, newFavorite];
};
