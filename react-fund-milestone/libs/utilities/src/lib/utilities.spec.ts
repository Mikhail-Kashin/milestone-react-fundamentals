import {
  shapeYelpArray,
  jacksonvilleBuisnessesArray,
  searchResult,
  jacksonvilleBuisnessesArrayWithIsFavorite,
} from './utilities';

import {
  jacksonvilleBuisnessesArrayTestExpectedObject,
  shappedYelpObj,
} from '@react-fund-milestone/test-data';

describe('jacksonvilleBuisnessesArray', () => {
  it('should be an array of business objects', () => {
    expect(jacksonvilleBuisnessesArray[0]).toEqual(
      jacksonvilleBuisnessesArrayTestExpectedObject
    );
  });
});

describe('shapeYelpArray', () => {
  it('should shape data from the jacksonvilleBuisnessesArray into the expected obejct format', () => {
    const actual = shapeYelpArray(jacksonvilleBuisnessesArrayWithIsFavorite);

    expect(actual[0]).toEqual(shappedYelpObj);
  });
});

describe('jacksonvilleBuisnessesArrayWithIsFavorite', () => {
  it('should add a key of isFavorite with value of false to ojects', () => {
    const firstObjc = jacksonvilleBuisnessesArrayWithIsFavorite[0];
    const secondObjc = jacksonvilleBuisnessesArrayWithIsFavorite[1];
    expect(firstObjc.isFavorite).toBeFalsy();
    expect(secondObjc.isFavorite).toBeFalsy();
  });
});

describe('searchResult', () => {
  it('should return the business with the matching name', () => {
    const actual = searchResult('maple');

    expect(actual[0]).toEqual(shappedYelpObj);
  });

  it('should return an empty array if result is not matching', () => {
    const actual = searchResult('9');

    expect(actual).toEqual([]);
  });
  it('should return an empty array if result is not matching', () => {
    const actual = searchResult('9');

    expect(actual).toEqual([]);
  });
});
